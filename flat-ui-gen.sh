awk '!/(^#|^$)/ {
		C= lshift($2,16) + lshift($3, 8) + $4;
		printf "\"#%x\", /* %2d: %s */\n", C, L, $1;
		L++;
		if (! (L % 8)) print "";
	}' <<EOF
# source: https://github.com/geekjuice/flat-ui-mintty/
# MIT License

Black              44,   62,  80
Red                192,  57,  43
Green              39,  174,  96
Yellow             243, 156,  18
Blue               41,  128, 185
Magenta            142,  68, 173
Cyan               122, 160, 133
White              189, 195, 199
BrightBlack        52,   73,  94
BrightRed          231,  76,  60
BrightGreen        46,  204, 113
BrightYellow       241, 196,  15
BrightBlue         52,  152, 219
BrightMagenta      155,  89, 182
BrightCyan         26,  188, 156
BrightWhite        236, 240, 241
Foreground         236, 240, 241
Background         24,   24,  24
Cursor             211,  84,   0
EOF

